﻿using System;
using System.Xml.Serialization;

namespace XmlToJson
{
    [Serializable]
    public class ProductOccurence
    {
        [XmlAttribute]
        public string Id { get; set; }

        [XmlAttribute]
        public string Name { get; set; }

        [XmlArray("Attributes"), XmlArrayItem(ElementName = "Attr", Type = typeof(Property))]
        public Property[] Props { get; set; }
    }
}
