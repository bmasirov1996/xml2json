﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace XmlToJson
{
    [XmlRoot("Root")]
    public class Root
    {
        [XmlArray("ModelFile")]
        [XmlArrayItem("ProductOccurence", typeof(ProductOccurence))]
        public ProductOccurence[] ProductOccurence { get; set; }
    }
}
