﻿using Newtonsoft.Json;
using System;
using System.IO;
using System.Xml.Serialization;

namespace XmlToJson
{
    class Program
    {
        private const string outputPath ="../../../result.json";
        private const string inputPath = "../../../test.xml";

        static  void Main(string[] args)
        {
            var productOccurence = GetProductOccurrence();

            WriteJsonStringToFile(ConvertToJson(productOccurence));
           

            Console.WriteLine("--ГОТОВО--"); 
            Console.ReadLine();
        }

        static ProductOccurence[] GetProductOccurrence()
        {
            XmlSerializer formatter = new XmlSerializer(typeof(Root));
            Root root = new Root();

            try
            {
                using (FileStream fs = new FileStream(inputPath, FileMode.OpenOrCreate))
                {
                    root = (Root)formatter.Deserialize(fs);
                }
            }
            catch (Exception ex)
            { 
                Console.WriteLine("Error: " + ex.Message); 
            }
            return root.ProductOccurence;
        }

        static string ConvertToJson(ProductOccurence[] result)
        {
            string json = JsonConvert.SerializeObject(result, Formatting.Indented);        
            return json;
        }   

        static void WriteJsonStringToFile(string jsonString)
        {
            try
            {
                using (var StreamWriter = new StreamWriter(outputPath))
                {
                    StreamWriter.Write(jsonString);
                }
            }
            catch (Exception ex) 
            {
                Console.WriteLine("Error: " + ex.Message);
            }
        }       
    }
}
